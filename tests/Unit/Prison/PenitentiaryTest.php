<?php
namespace Tests\Unit\Mafia\Member;

use PHPUnit\Framework\TestCase;
use Mafia\Member\Domain\Mobster;
use Prison\Domain\Penitentiary;

class PenitentiaryTest extends TestCase
{
    public function test_penitentiary_add_prisoner()
    {
        $mobster = new Mobster(1, 37);

        $penitentiary = Penitentiary::getInstance();
        $penitentiary->addPrisoner($mobster);

        $prisoners = $penitentiary->prisoners();
        $prisoner = array_pop($prisoners);
        
        $this->assertEquals($prisoner, $mobster);
    }

    public function test_penitentiary_release_prisoner()
    {
        $penitentiary = Penitentiary::getInstance();

        //Check the instance of the singleton contains the prisoner of the previous test
        $this->assertCount(1, $penitentiary->prisoners());

        $mobster = $penitentiary->releasePrisoner(1);

        //Check the penitentiary is now empty
        $this->assertCount(0, $penitentiary->prisoners());

        //Check the released mobster is correct
        $this->assertInstanceOf(Mobster::class, $mobster);
        $this->assertEquals($mobster->id(), 1);
        $this->assertEquals($mobster->age(), 37);
    }
/*
    public function test_penitentiary_unic_singleton()
    {
        
    }*/
}