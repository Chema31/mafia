<?php
namespace Tests\Unit\Mafia\Member;

use PHPUnit\Framework\TestCase;
use Mafia\Member\Domain\Mobster;

class MobsterTest extends TestCase
{
    public function test_mobster_creation()
    {
        $id = random_int(1,100);
        $age = random_int(18, 65);

        $mobster = new Mobster($id, $age);

        //Check the object type
        $this->assertInstanceOf(Mobster::class, $mobster);

        //Check ID and age
        $this->assertEquals($id, $mobster->id());
        $this->assertEquals($age, $mobster->age());
    }

    public function test_mobster_add_subordinates()
    {
        $mobster = new Mobster(1, 37);

        for ($id = 2; $id < 6; $id++) {
            $subordinate = new Mobster($id, (20+$id));

            $mobster->addSubordinate($subordinate);
        }
        
        $subordinates = $mobster->subordinates();
        
        //Check number of subordinates
        $this->assertCount(4, $subordinates);

        //Check subordinates
        for ($id = 2; $id < 6; $id++) {
            $this->assertInstanceOf(Mobster::class, $subordinates['member_'.$id]);
            $this->assertEquals($subordinates['member_'.$id]->id(), $id);
            $this->assertEquals($subordinates['member_'.$id]->age(), (20+$id));
        }
    }

    public function test_mobster_add_prisonerBossId()
    {
        $mobster = new Mobster(2, 22);
        $mobster->setPrisonerBossId(1);

        $this->assertEquals($mobster->prisonerBossId(), 1);
    }
}