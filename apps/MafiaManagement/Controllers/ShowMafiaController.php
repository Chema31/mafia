<?php
namespace Apps\MafiaManagement\Controllers;

use Mafia\Member\Services\ClanService;
use Mafia\Member\Presentation\Presentation;

class ShowMafiaController
{
    private $clanService;
    private $clanPresentation;

    public function __construct(ClanService $clanService, Presentation $clanPresentation)
    {
        $this->clanService = $clanService;
        $this->clanPresentation = $clanPresentation;
    }

    public function __invoke()
    {
        $clan = $this->clanService->getClan();
        $this->clanPresentation->render($clan);
    }
}