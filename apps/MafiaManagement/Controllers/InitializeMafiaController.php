<?php
namespace Apps\MafiaManagement\Controllers;

use Mafia\Member\Services\ClanService;
use Mafia\Member\Presentation\Presentation;

class InitializeMafiaController
{
    private $clanService;
    private $clanPresentation;

    public function __construct(ClanService $clanService, Presentation $clanPresentation)
    {
        $this->clanService = $clanService;
        $this->clanPresentation = $clanPresentation;
    }

    public function __invoke()
    {
        $clan = $this->clanService->initializeMafiaService();

        $this->clanPresentation->render($clan);
    }
}