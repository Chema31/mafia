<?php
namespace Prison\Interfaces;

use Mafia\Member\Interfaces\Criminal;

interface Jail
{
    public function addPrisoner(Criminal $criminal): Criminal;
    public function releasePrisoner(int $criminalId): Criminal;
}