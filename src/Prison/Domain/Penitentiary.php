<?php
/**
 * Singletone pattern which implements the Jail interface.
 * In this way, we are always sure that only exist one instance of the Penitentiary.
 */
namespace Prison\Domain;


use Prison\Interfaces\Jail;
use Mafia\Member\Interfaces\Criminal;

class Penitentiary implements Jail
{
    private static $instance = null;

    private static $keyPrefix = 'prisoner_';   //Prefix to create the key in the array of prisoners
    protected $prisoners = [];

    protected function __construct() { }
    protected function __clone() { }
    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * Get the unic instance of the Penitentiary
     * @return Penitentiary
     */
    public static function getInstance(): Penitentiary
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function addPrisoner(Criminal $criminal): Criminal
    {
        $this->prisoners[self::$keyPrefix.$criminal->id()] = $criminal;

        return $this->prisoners[self::$keyPrefix.$criminal->id()];
    }

    public function releasePrisoner(int $criminalId): Criminal
    {
        $prisoner = $this->prisoners[self::$keyPrefix.$criminalId];

        unset($this->prisoners[self::$keyPrefix.$prisoner->id()]);

        return $prisoner;
    }

    public function prisoners()
    {
        return $this->prisoners;
    }
}