<?php
//TODO
namespace Mafia\Member\Services;

use Mafia\Member\Domain\Capo;
use Lib\Persistence\Persistence;
use Mafia\Member\Domain\Mobster;
use Mafia\Member\Interfaces\Boss;

class ClanService
{
    protected $persistence;

    public function __construct(Persistence $persistence)
    {
        $this->persistence = $persistence;
    }

    public function initializeMafiaService()
    {
        $boss = new Capo(1, 37);

        $this->makeClan($boss, 3, 3);

        $this->persistence->save($boss);

        return $boss;
    }

    protected function makeClan(Boss $boss, int $dept, int $with)
    {
        if ($dept != 0) {
            for($i=0; $i<$with; $i++){
                $newBoss = new Capo(uniqid(), (50+$i));
                $boss->addSubordinate($newBoss);
                $this->makeClan($newBoss, ($dept-1), $with);
            }
            
        } else {
            for($i=0; $i<$with; $i++){
                $newMobster = new Mobster(uniqid(), (20+$i));
                $boss->addSubordinate($newMobster);
            }
        }
    }

    public function getClan(): Boss
    {
        return $this->persistence->get();
    }

    /**
     * Search for member with the ID passed as parameter
     *
     * @param int $id
     * @return Criminal
     */
    /*
    public function searchMemberByIdIntoSubordinates(int $id): Criminal
    {
        $subordinates = $this->getSubordinates();

        if($subordinates){
            foreach ($subordinates as $subordinate){
                if( $subordinate->getId() == $id) {
                    return $subordinate;

                } else {
                    return $this->searchMemberByIdIntoSubordinates($id);
                }
            }
        }
    }
*/
    /**
     * Set the member substitute when he is sent to jail
     */
  /*  public function setSubstitute()
    {
        $possibleSubstitutes = $this->getBoss()->getSubordinates();

        if( $possibleSubstitutes ){ //Look for oldest substitute with the same level (same boss)
            $this->getBoss()->setSubstituteWithTheOldestSubordinate($this->getId());

        } else {    //It is not possible with the same level member so it will be a promotion
            $this->setSubstituteWithTheOldestSubordinate();
        }

        //Take the actions of substitution
        if( $this->substitute ){
            $this->substitute->setBoss($this->getBoss());

            foreach($this->getSubordinates() as $subordinate){
                $this->substitute->addSubordinate($subordinate);
            }
            $this->getBoss()->removeSubordinate($this);
        }

    }

    /**
     * Look for the appropiate substitute
     * @param int|null $ownId
     */
 /*   protected function setSubstituteWithTheOldestSubordinate( int $ownId = null )
    {
        $possibleSubstitutes = $this->getSubordinates();

        foreach ($possibleSubstitutes as $possible){
            if($ownId != $possible->getId()){

                if(!$this->substitute){
                    $this->substitute = $possible;

                } else {
                    if($this->substitute->getAge() < $possible->getAge()) {
                        $this->substitute = $possible;  //The oldest
                    }
                }
            }

        }
    }*/
}