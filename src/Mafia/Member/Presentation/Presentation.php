<?php
namespace Mafia\Member\Presentation;

use Mafia\Member\Interfaces\Boss;

interface Presentation
{
    public function render(Boss $clan);
}