<?php
//TODO
namespace Mafia\Member\Presentation\Implementations;

use Mafia\Member\Interfaces\Boss;
use Mafia\Member\Presentation\Presentation;
use Mafia\Member\Presentation\Implementations\ClanPresentation;

class JsonClanPresentation extends ClanPresentation implements Presentation
{
    protected function arrayToJson(array $clanArray)
    {
        return json_encode($clanArray, JSON_PRETTY_PRINT);
    }

    public function render(Boss $capo)
    {
        $clanArray = $this->toArray($capo);

        echo $this->arrayToJson($clanArray);
    }
}