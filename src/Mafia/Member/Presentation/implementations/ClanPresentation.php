<?php
//TODO
namespace Mafia\Member\Presentation\Implementations;

use Mafia\Member\Interfaces\Boss;
use Mafia\Member\Interfaces\Criminal;

class ClanPresentation
{
    public function toArray(Boss $capo): array
    {
        $node = [
            'id' => $capo->id(),
            'age' => $capo->age(),
            'status' => get_class($capo),
            'subordinates' => []
        ];
        $subordinates = $capo->subordinates();

        if ($subordinates) {
            foreach ($subordinates as $subordinate) {
                $node['subordinates'][] = $this->fillSubordinates($subordinate);
            }
        }

        return $node;
    }

    private function fillSubordinates(Criminal $criminal)
    {
        $node = [
            'id' => $criminal->id(),
            'age' => $criminal->age(),
            'status' => get_class($criminal),
            'subordinates' => []
        ];
        if ($criminal instanceof Boss) {
            $subordinates = $criminal->subordinates();

            if ($subordinates) {
                foreach ($subordinates as $subordinate) {
                    $node['subordinates'][] = $this->fillSubordinates($subordinate);
                }
            }
        }

        return $node;
    }
}