<?php
/**
 * Capo is the complex node of the Composite pattern in order to manage the tree of the criminal organization.
 */
namespace Mafia\Member\Domain;

use Mafia\Member\Interfaces\Boss;
use Mafia\Member\Interfaces\Criminal;

class Capo extends Mobster implements Boss
{
    private static $keyPrefix = 'member_';   //Prefix to create the key in the array of subordinates

    protected $subordinates;

    public function __construct(string $id, int $age){
        parent::__construct($id, $age);
        $this->subordinates = [];
    }

    public function subordinates(): array
    {
        return $this->subordinates;
    }

    public function addSubordinate(Criminal $subordinate): Criminal
    {
        $this->subordinates[self::$keyPrefix.$subordinate->id()] = $subordinate;
        $subordinate->setBoss($this);

        return $subordinate;
    }
}