<?php
/**
 * Mobster is the leaf node of the Composite pattern in order to manage the tree of the criminal organization.
 */
namespace Mafia\Member\Domain;

use Mafia\Member\Interfaces\Boss;
use Mafia\Member\Interfaces\Criminal;

class Mobster implements Criminal
{
    protected $id;
    protected $age;
    protected $boss;
    protected $prisonerBossId; //Boss Id who is temporary in jail

    public function __construct(string $id, int $age){
        $this->id = $id;
        $this->age = $age;
        $this->prisonerBossId = null;
    }

    public function id(): string
    {
        return $this->id;
    }

    public function age(): int
    {
        return $this->age;
    }

    public function boss(): Boss
    {
        return $this->boss;
    }

    public function prisonerBossId(): ?int
    {
        return $this->prisonerBossId;
    }

    public function setBoss(Boss $boss): ?Boss
    {
        //$boss->addSubordinate($this); //To avoid infinite loops, the subordinates alwais have to be stablished the Boss implementation
        $this->boss = $boss;

        return $this->boss;
    }

    public function setPrisonerBossId(int $prisonerBossId): ?int
    {
        $this->prisonerBossId = $prisonerBossId;

        return $this->prisonerBossId;
    }
}