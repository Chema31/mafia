<?php
namespace Mafia\Member\Interfaces;

interface Criminal
{
    public function __construct(string $id, int $age);
    public function id(): string;
    public function age(): int;
    public function boss(): ?Boss;
    public function prisonerBossId(): ?int;
    public function setBoss(Boss $boss): ?Boss;
    public function setPrisonerBossId(int $prisonerBossId): ?int;
}
