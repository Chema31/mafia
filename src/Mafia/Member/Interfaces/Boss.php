<?php
namespace Mafia\Member\Interfaces;

interface Boss
{
    public function subordinates(): array;
    public function addSubordinate(Criminal $subordinate): Criminal;
}