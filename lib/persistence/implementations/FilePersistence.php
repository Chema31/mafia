<?php
namespace Lib\Persistence\Implementations;

use Dotenv\Dotenv;
use Exception;
use Lib\Persistence\Persistence;
use Mafia\Member\Interfaces\Boss;

class FilePersistence implements Persistence
{
    protected $mafiaPath;

    public function __construct()
    {
        $this->mafiaPath = $_ENV['MAFIA_STORAGE_PATH'];
    }

    public function save(Boss $clan)
    {
        try {
            $f = fopen($this->mafiaPath,'w');
            fwrite($f, serialize($clan));
            fclose($f);

        } catch(Exception $e) {
            echo "\nError al persistir los datos en fichero.\n";
            echo $e->getMessage();
        }
    }

    public function get(): Boss
    {
        $f = fopen($this->mafiaPath,'r');
        $clan = unserialize( fgets($f) );
        fclose($f);

        return $clan;
    }
}