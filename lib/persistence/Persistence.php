<?php
namespace Lib\Persistence;

use Mafia\Member\Interfaces\Boss;

interface Persistence
{
    public function save(Boss $clan);
    public function get(): Boss;
}