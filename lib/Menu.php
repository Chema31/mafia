<?php
namespace Lib;

use Mafia\Member\Services\ClanService;
use Mafia\Member\Presentation\ClanPresentation;
use Apps\MafiaManagement\Controllers\ShowMafiaController;
use Apps\PrisonManagement\Controllers\ShowPrisonController;
use Apps\MafiaManagement\Controllers\InitializeMafiaController;
use Apps\PrisonManagement\Controllers\AddCriminalToPrisonController;
use Apps\PrisonManagement\Controllers\ReleaseCriminalFromPrisonController;
use Lib\Persistence\Implementations\FilePersistence;
use Mafia\Member\Presentation\Implementations\JsonClanPresentation;

Class Menu
{
    public function printOptions()
    {
        echo "Debe seleccionar un comando entre los existentes:\n";
        echo "1. init\n";
        echo "2. add-prisoner\n";
        echo "3. release-prisoner\n";
        echo "4. show-mafia\n";
        echo "5. show-prison\n";
    }

    public function readOption(array $argv)
    {
        if (isset($argv[1])) {
        
            //Dependencies Injection
            $persistence = new FilePersistence();
            
            $clanService = new ClanService($persistence);
            $clanPresentation = new JsonClanPresentation();

            //Routing actions
            switch ($argv[1]) {
                case 'init':
                    call_user_func(new InitializeMafiaController($clanService, $clanPresentation));
                    break;

                case 'add-prisoner':
                    call_user_func(new AddCriminalToPrisonController());
                    break;

                case 'release-prisoner':
                    call_user_func(new ReleaseCriminalFromPrisonController());
                    break;

                case 'show-mafia':
                    call_user_func(new ShowMafiaController($clanService, $clanPresentation));
                    break;

                case 'show-prison':
                    call_user_func(new ShowPrisonController());
                    break;

                default:
                    $this->printOptions();
            }
        } else {
            $this->printOptions();
        }
    }
}