# Mafia

## Intro

Este código se ha creado como ejemplo de codificación, basado en una implementación a una prueba técnica.

Intento crear un desarrollo **sin Frameworks**, respetando al máximo los principios **SOLID**, con **DDD** y **Arquitectura Hexagonal**. Además, hago uso de los patrones de diseño **Singleton** y **Composite**.

Mi intención es aprovechar una temática divertida y con suficiente versatilidad como para ampliarlo si alguna vez dispongo de tiempo.

## Actualmente desarrollado

Actualmente se ha desarrollado la capa principal de Infraestructura y Dominio de forma que ya funcionan algunos comandos.

También, están creadas las estructuras de datos básicas de *Composite* para el *Clan mafioso* y el *Singleton* de la prisión, los métodos con recursividad para sus acciones y disponiendo de una cobertura de test básica.

Además, se ha desarrollado una capa de persistencia totalmente desacoplada, que actualmente se hace sobre fichero, pero que en caso de modificarse a base de datos u otra, no afectaría en ningún caso a la lógica de negocio y únicamente supondría la implementación de una nueva clase.

## Por desarrollar

Faltan por desarrollar las acciones que suponen movimientos de nodos y ramas de la estructura de datos en árbol, cuya representación se detalla a continuación.

## ¿En qué consiste?

El código intenta representar una estructura criminal mafiosa sobre la que pueden darse diferentes sucesos.

Partimos de que la mafia es dirigida por un capo y cuenta con multitud de subordinados en una estructura jerárquica con forma de pirámide. Al final, cada subordinado cuenta con más criminales a su cargo, en diferentes niveles de jerarquía.

Actualmente, se encuentra implementada la creación de la estructura, sus tests, la persistencia en fichero y la ejecución del comando.

En el futuro, se esperan desarrollar las siguientes acciones:

### 1. ¿Que ocurre cuando un criminal es capturado y entra en prisión?

 * En ese momento todos los subordinados del criminal capturado pasarán a ser subordinados del compañero de mayor edad, que se encuentre en el mismo nivel de jerarquía que el criminal que ha entrado en prisión. Si el criminal capturado no contase con ningún compañero del mismo nivel de jerarquía, los subordinados pasarán directamente al responsable inmediato del criminal capturado.

### 2. ¿Que ocurre cuando un criminal sale de prisión?

* En este caso, el criminal que sale de prisión volverá a su posición jerárquica anterior y recuperará a sus subordinados.

## ¿Como se ejecuta?

Se trata de una aplicación PHP para CLI.
Necesita tener instalada una versión de PHP >= 7.1 para consola de comandos.

Para arrancar la aplicación únicamente es necesario ejecutar el comando "./mafia" seguido de la opción en texto que se desea ejecutar. Por ejemplo: **./mafia init**

Actualmente, dispone de las siguientes opciones:
  - *init*
  - *add-prisoner*
  - *release-prisoner*
  - *show-mafia*
  - *show-prison*